package com.example.dagverxa

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.CheckBox
import android.widget.EditText
import android.widget.Toast
import androidx.core.text.isDigitsOnly

class MainActivity : AppCompatActivity() {

    private lateinit var test: EditText
    private lateinit var test1: EditText
    private lateinit var test2: EditText

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        test = findViewById(R.id.test)
        test1 = findViewById(R.id.test1)
        test2 = findViewById(R.id.test2)

    }

    fun btn(clickedView: View) {

        if (test1.text.isDigitsOnly() && test1.text.length == 3) {

            if (test1.text.toString() != "000") {

                Toast.makeText(applicationContext, "success", Toast.LENGTH_SHORT).show()

            }

        } else {
            Toast.makeText(applicationContext, "error", Toast.LENGTH_SHORT).show()
        }

    }

}